- Make sure you have virtualenv installed. (i.e. `sudo pip install virtualenv`)
- Create you virtualenv `virtualenv venv`
- Activate your virtualenv by running `source venv/bin/activate`
- Install requirements by running `pip install -r requirements.txt`
- Run `python Travestator.py` which will listen for traffic on `http://0.0.0.0:5000`.


#### OSX post install instructions
add and install fonts to Font Book

run in terminal

`cd /`

`sudo /usr/libexec/locate.updatedb`

`wget http://www.imagemagick.org/Usage/scripts/imagick_type_gen`

`perl imagick_type_gen > ~/.magick/type.xml`

`brew install Caskroom/cask/xquartz`

`brew install python cairo pango gdk-pixbuf libxml2 libxslt libffi`

#### Linux post install instructions
##### Long list of dependencies
```
sudo apt-get install software-properties-common python-software-properties
sudo add-apt-repository ppa:tycho-s/ppa
sudo apt-get update
sudo apt-get install python python-gobject python-gi python-cairo python-gi-cairo \
  gir1.2-pango gir1.2-gtk xdg-utils  \
  librsvg2-common python-dev libglib2.0-dev python-gobject-dev libgirepository1.0-dev \
  libcairo2-dev python-cairo-dev intltool python-pyicu \
  rcs ttf-freefont python-webkit python-pygoocanvas gir1.2-goocanvas-2.0-9 \
  gtk2-engines-pixbuf libmagickwand-dev imagemagick
```

##### Install wordlist
`sudo apt-get install --reinstall wamerican`

copy fonts to `~/.fonts`




