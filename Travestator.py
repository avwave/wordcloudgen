from random import randint
from datetime import datetime
import os, uuid
from flask import Flask, render_template, send_file, request, url_for, abort, send_from_directory
from flask.ext.moment import Moment
from flask.ext.sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap

from Travesty import Travesty
import sys
import logging
from generator import ImageGenerator
import cairo_draw
import json
import web

APP_ROOT = os.path.dirname(os.path.abspath(__file__))  # refers to application_top
APP_STATIC = os.path.join(APP_ROOT, 'static')
IMAGES_ROOT = os.path.join(APP_STATIC, 'images')

app = Flask(__name__)

basedir = os.path.abspath(os.path.dirname(__file__))

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'urls.sqlite')
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
db = SQLAlchemy(app)

app.logger.addHandler(logging.StreamHandler(sys.stdout))
app.logger.setLevel(logging.ERROR)

app.config['BOOTSTRAP_SERVE_LOCAL'] = True
bootstrap = Bootstrap(app)
moment = Moment(app)

travesty = Travesty()
markov_length = 3

class Bot(object):
    def __init__(self, name):
        print 'loading ' + name + '...'
        self.base_folder = 'static/persona/' + name + '/'
        self.flask_base_folder = 'persona/' + name + '/'

        with app.open_resource(self.base_folder + 'config.json') as f:
            j = json.load(f, parse_int=int)
            self.author = j['author']
            self.handle = j['handle']
            self.author_font = j['author_font']
            self.background_color = j['background_color']
            self.base_text_color = j['base_text_color']
            self.highlight_color = j['highlight_color']
            self.text_font = j['text_font']
            self.complexity = j['complexity']

            with app.open_resource(self.base_folder + 'brains.txt') as f:
                self.brains = Travesty()
                self.brains.buildMapping(travesty.wordlist(f), self.complexity)
        print 'loading ' + name + '[finished]'




with app.app_context():
    cevbot = Bot('cevbot')
    avbot = Bot('avbot')
    archerbot = Bot('archerbot')

class Urls(db.Model):
    local_uid = db.Column(db.Text, primary_key=True)
    public_uid = db.Column(db.Text)
    pub_date = db.Column(db.DateTime, default=datetime.utcnow)
    count = db.Column(db.Integer, default=0)

    def __str__(self):
        return self.http_url

db.create_all()


@app.route('/')
@app.route('/', subdomain='www.bl4g.com')
@app.route('/', subdomain='twatter.actualbrain.com')
def say_avbot():
    message = avbot.brains.genSentence(avbot.complexity)

    url = generate_image(message, avbot)

    local_url = url_for('static', filename='images/' + url.local_uid + '.png')
    public_url = url_for('decode_url', image_url=url.public_uid)

    return render_template('index.html', message=message, current_time=datetime.utcnow(), public_url=public_url,
                           local_url=local_url, persona=avbot)



@app.route('/archer')
@app.route('/archer', subdomain='twatter.actualbrain.com')
def hello_lana():
    message = archerbot.brains.genSentence(archerbot.complexity)

    url = generate_image(message, archerbot)

    local_url = url_for('static', filename='images/' + url.local_uid + '.png')
    public_url = url_for('decode_url', image_url=url.public_uid)

    return render_template('index.html', message=message, current_time=datetime.utcnow(), public_url=public_url,
                           local_url=local_url, persona=archerbot)


@app.route('/cevius')
@app.route('/cevius', subdomain='twatter.actualbrain.com')
def hello_cevius():
    message = cevbot.brains.genSentence(10)

    url = generate_image(message, cevbot)

    local_url = url_for('static', filename='images/' + url.local_uid + '.png')
    public_url = url_for('decode_url', image_url=url.public_uid)

    return render_template('index.html', message=message, current_time=datetime.utcnow(), public_url=public_url,
                           local_url=local_url, persona=cevbot)



def generate_filename():
    import re
    word_file = "/usr/share/dict/words"
    WORDS = open(word_file).read().splitlines()
    word_count = len(WORDS)

    word_range1 = randint(1, word_count)
    word_range2 = randint(word_range1, word_count)

    word1 = WORDS[word_range1].replace("'", "")
    word2 = WORDS[word_range2].replace("'", "")

    filename = word1 + word2
    url = Urls.query.filter_by(public_uid=filename).first()

    if url:
        return generate_filename()
    else:
        return filename


@app.route('/image/<string:image_url>')
def decode_url(image_url):
    url = Urls.query.filter_by(public_uid=image_url).first()
    if url:
        return send_from_directory(IMAGES_ROOT, url.local_uid + '.png')

    abort(404)


def generate_image(message, persona):
    image_Generator = ImageGenerator(
        message)

    url = Urls()
    url.public_uid = generate_filename()
    url.local_uid = str(uuid.uuid4())

    quote = cairo_draw.Quote(message, author=persona.author, author_font=persona.author_font,
                     output_file=os.path.join(IMAGES_ROOT, url.local_uid + '.png'))

    cairo_draw.logo_path = os.path.join(APP_STATIC, 'logo.svg')
    cairo_draw.generate_image(quote)

    db.session.add(url)
    db.session.commit()

    return url


if __name__ == '__main__':
    print os.path.join(APP_STATIC, 'source.txt')
    # travesty.buildMapping(travesty.wordlist(os.path.join(APP_STATIC, 'source.txt')), markov_length)
    # travesty.buildMapping(travesty.wordlist('static/source.txt'), markov_length)
    #app.run(debug=True)
    app.run(host='0.0.0.0', port=5001)

