import cairo
import pango
import pangocairo
import struct
import sys
import gtk


from Travesty import Travesty

from random import randrange

def hex_to_RGB_tuple(hexstring):
    return struct.unpack('BBB', hexstring.decode('hex'))

padding = 50
text_bounds = 500
image_width = text_bounds + (padding * 2)

logo_path = 'static/logo.svg'

class Quote(object):
    def __init__(self, text='', author='', background_color='000000', base_text_color='ffffff', highlight_color='ff0000', text_font='Raleway', author_font='Raleway', output_file='output.png'):
        self.background_color = background_color
        self.base_text_color = base_text_color
        self.highlight_color = highlight_color
        self.text = text
        self.author = author
        self.author_font = author_font
        self.text_font = text_font
        self.output_file = output_file

def generate_image(quote):
    wordlist = quote.text.split()
    randomindex = randrange(0, len(wordlist))
    wordlist[randomindex] = "<span foreground='#" + quote.highlight_color + "'>" + wordlist[randomindex] + "</span>"

    randomindex = randrange(0, len(wordlist))
    wordlist[randomindex] = "<i>" + wordlist[randomindex] + "</i>"

    quote.text = "<span foreground='#" + quote.base_text_color + "'>" + ' '.join(wordlist) + "</span>"

    surface = cairo.RecordingSurface(cairo.FORMAT_ARGB32, None)
    cairocontext = cairo.Context(surface)

    context = gtk.gdk.CairoContext(cairocontext)

    #init canvas
    context.set_antialias(cairo.ANTIALIAS_SUBPIXEL)


    #init fonts/pango
    font_name = quote.text_font
    pangocairo_context = pangocairo.CairoContext(context)
    pangocairo_context.set_antialias(cairo.ANTIALIAS_SUBPIXEL)

    quote_layout = pangocairo_context.create_layout()

    font = pango.FontDescription(font_name)
    font.set_size(24 * pango.SCALE)
    font.set_weight(pango.WEIGHT_SEMIBOLD)
    quote_layout.set_font_description(font)


    context.identity_matrix()
    context.translate(padding, padding)
    font.set_weight(pango.WEIGHT_SEMIBOLD)
    quote_layout.set_font_description(font)

    quote_layout.set_alignment(pango.ALIGN_CENTER)
    quote_layout.set_width(text_bounds * pango.SCALE)
    quote_layout.set_wrap(pango.WRAP_WORD)
    quote_layout.set_markup(quote.text)


    new_height = (int)(quote_layout.get_pixel_extents()[1][3] + (padding * 2.5))

    context.rectangle(-padding, -padding, image_width, new_height)
    rgb = hex_to_RGB_tuple(quote.background_color)
    context.set_source_rgb(rgb[0], rgb[1], rgb[2])
    context.fill()

    pangocairo_context.update_layout(quote_layout)
    pangocairo_context.show_layout(quote_layout)


    author_layout = pangocairo_context.create_layout()
    font.set_family(quote.author_font)
    font.set_size(12 * pango.SCALE)
    font.set_weight(pango.WEIGHT_NORMAL)
    author_layout.set_font_description(font)

    author_layout.set_alignment(pango.ALIGN_RIGHT)
    author_layout.set_width((image_width - padding - 10) * pango.SCALE)
    author_layout.set_wrap(pango.WRAP_WORD)
    author_layout.set_markup("<span foreground='#" + quote.background_color + "'> -- " + quote.author + "</span>")

    context.translate(0, new_height - (padding * 1.5))
    context.set_source_rgb(1,1,1)
    context.rectangle(-padding, 0, image_width, padding)
    context.fill()

    pangocairo_context.update_layout(author_layout)
    pangocairo_context.show_layout(author_layout)


    #draw svg attribution
    logo_attrib = gtk.gdk.pixbuf_new_from_file_at_scale(logo_path, 100, 100, preserve_aspect_ratio=True)
    context.set_source_pixbuf(logo_attrib, -padding + 2, 5)
    context.paint()

    out_surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, image_width, new_height)
    out_context = cairo.Context(out_surface)

    pattern = cairo.SurfacePattern(surface)

    out_context.identity_matrix()
    out_context.set_source(pattern)
    out_context.paint()

    with open(quote.output_file, 'wb') as image_file:
        out_surface.write_to_png(image_file)


if __name__ == '__main__':
    quote = Quote(author='Sterling Archerbot', author_font='Baveuse')

    travesty = Travesty()
    with open('static/archer1_10.txt', 'r') as f:
        travesty.buildMapping(travesty.wordlist(f), 3)

    quote.text = travesty.genSentence(3)
    generate_image(quote)
