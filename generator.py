import os
import textwrap
from wand.drawing import Drawing, FontMetrics
from wand.color import Color
from wand.image import Font
from wand.image import Image

from sys import platform as _platform

class ImageGenerator(object):
    quote_string = ""
    font = None
    text_padding = 50
    text_columns = 25
    line_spacing = 10
    text_size = 40
    target_size = 500

    def __init__(self, quote_string):
        self.quote_string = quote_string


    def draw_pullable(self, image_path):
        blob_str = ''

        multi_line = "\n".join(textwrap.wrap(self.quote_string, width=self.text_columns))
        y = 0
        x = 0

        text_bounds = self.target_size - (self.text_padding * 2)
        text_image = Image(width=text_bounds, height=text_bounds)

        drawer = Drawing()
        drawer.font_size = self.text_size

        drawer.font = 'Raleway-SemiBold'
        if _platform == "darwin":
            drawer.font = 'RalewaySemiBold'

        drawer.fill_color = Color('white')

        metrics = drawer.get_font_metrics(text_image, multi_line, multiline=True)

        image_result = Image(width=int(metrics.text_width + (self.text_padding * 2)),
                             height=int(metrics.text_height + self.text_padding * 2),
                             background=Color('black'))

        drawer.text_alignment = 'center'
        drawer.text(int(metrics.text_width/2) + self.text_padding,
                    self.text_size + self.text_padding,
                    multi_line)

        drawer.font_size = self.text_size * 3
        metrics = drawer.get_font_metrics(text_image, "\"", multiline=False)
        drawer.text_alignment = 'left'
        drawer.text(0,
                    int(metrics.ascender),
                    u"\u201D")

        drawer.text_alignment = 'right'
        drawer.text(image_result.width,
                    image_result.height - int(metrics.descender),
                    u"\u201C")

        drawer.draw(image_result)

        datafile = open(image_path, "w")
        datafile.write(image_result.make_blob('png'))
        datafile.close()
